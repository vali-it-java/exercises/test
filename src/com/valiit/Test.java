package com.valiit;

import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {

        System.out.println("Solution 1");
        double pi = Math.PI;
        System.out.println(2 * pi);

        System.out.println();
        System.out.println("Solution 2");
        System.out.printf("Rectangle with sides %d and %d has area %d", 2, 3, calculateRectangleArea(2, 3));
        System.out.println();

        System.out.println();
        System.out.println("Solution 3");
        System.out.println("Censored words: ");
        String[] words = {"auto", "rebane", "sau"};
        String[] censoredWords = censorWords(words);
        for (String censoredWord : censoredWords) {
            System.out.println(" " + censoredWord);
        }

        System.out.println();
        System.out.println("Solution 4");
        System.out.println("Weather descriptions:");
        System.out.println(getWeatherDescription(25));
        System.out.println(getWeatherDescription(31));
        System.out.println(getWeatherDescription(6));
        System.out.println(getWeatherDescription(-4));
        System.out.println(getWeatherDescription(11));
        System.out.println(getWeatherDescription(14));
        System.out.println(getWeatherDescription(17));
        System.out.println(getWeatherDescription(22));

        System.out.println();
        System.out.println("Solution 5");

        Song song = new Song();
        song.setTitle("Poker Face");
        song.setArtist("Lady Gaga");
        song.setAlbum("The Fame");
        song.setReleased(2008);
        song.getGenres().add("Dance Pop");
        song.getGenres().add("Pop");
        song.getGenres().add("Modern");
        song.setLyrics("I wanna hold 'em like they do in Texas plays\nFold 'em, let 'em, hit me, raise it baby stay with me (I love it)\nLove Game intuition play the cards with spades to start\nAnd after he's been hooked I'll play the one that's on his heart\nOh, oh, oh, oh, oh, oh-oh-e-oh-oh-oh\nI'll get him hot, show him what I've got\nOh, oh, oh, oh, oh, oh-oh-e-oh-oh-oh,\nI'll get him hot, show him what I've got\nCan't read my\nCan't read my\nNo he can't read my poker face\n(She's got me like nobody)\nCan't read my\nCan't read my\nNo he can't read my poker face\n(She is gonna let nobody)\nP p p poker face, p p p poker face\n(Muh muh muh muh)\nP p p poker face, p p poker face\n(Muh muh muh muh)\nI wanna roll with him a hard pair we will be\nA little gambling is fun when you're with me (I love it)\nRussian Roulette is not the same without a gun\nAnd baby when it's love, if its not rough it isn't fun, fun\nOh, oh, oh, oh, oh, oh-oh-e-oh-oh-oh\nI'll get him hot, show him what I've got\nOh, oh, oh, oh, oh, oh-oh-e-oh-oh-oh,\nI'll get him hot, show him what I've got\nCan't read my\nCan't read my\nNo he can't read my poker face\n(She's got me like nobody)\nCan't read my\nCan't read my\nNo he can't read my poker face\n(She is gonna let nobody)\nP p p poker face, p p p poker face\n(Muh muh muh muh)\nP p p poker face, p p p poker face\n(Muh muh muh muh)\nI won't tell you that I love you\nKiss or hug you\n'Cause I'm bluffing with my muffin\nI'm not lying I'm just stunning with my love glue gunnin'\nJust like a chick in the casino\nTake your bank before I pay you out\nI promise this, promise this\nCheck this hand cause I'm marvelous\nCan't read my\nCan't read my\nNo he can't read my poker face\n(She's got me like nobody)\nCan't read my\nCan't read my\nNo he can't read my poker face\n(She is gonna let nobody)\nCan't read my\nCan't read my\nNo he can't read my poker face\n(She's got me like nobody)\nCan't read my\nCan't read my\nNo he can't read my poker face\n(She is gonna let nobody)\nCan't read my,\nCan't read my,\nNo he can't read my poker face\n(She's got me like nobody)\nCan't read my\nCan't read my\nNo he can't read my poker face\n(She is gonna let nobody)\nP p p poker face, p p p poker face\n(Muh muh muh muh)\nP p p poker face, p p p poker face\n(Muh muh muh muh)\nP p p poker face, p p p poker face\n(Muh muh muh muh)\nP p p poker face, p p p poker face\n(Muh muh muh muh)\nP p p poker face, p p p poker face\n(Muh muh muh muh)\nP p p poker face, p p p poker face\n(Muh muh muh muh)");

        System.out.println("The song:");
        System.out.println(song);
    }

    private static int calculateRectangleArea(int x, int y) {
        return x * y;
    }

    private static String[] censorWords(String[] words) {
        String[] response = new String[words.length];

        for (int i = 0; i < response.length; i++) {
            response[i] = censorWord(words[i]);
        }

        return response;
    }

    private static String censorWord(String word) {
        String result = "";
        for (int i = 0; i < word.length(); i++) {
            result += "#";
        }
        return result;
    }

    private static String getWeatherDescription(int temperature) {
        if (temperature > 30) {
            return "Leitsak";
        } else if (temperature >= 25) {
            return "Rannailm";
        } else if (temperature >= 21) {
            return "Mõnus suveilm";
        } else if (temperature >= 15) {
            return "Jahe suveilm";
        } else if (temperature >= 10) {
            return "Jaani-ilm";
        } else if (temperature >= 0) {
            return "Telekavaatamise ilm";
        } else {
            return "Suusailm";
        }
    }

    public static class Song {

        private String title;
        private String artist;
        private String album;
        private int released;
        private List<String> genres = new ArrayList<>();
        private String lyrics;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getArtist() {
            return artist;
        }

        public void setArtist(String artist) {
            this.artist = artist;
        }

        public String getAlbum() {
            return album;
        }

        public void setAlbum(String album) {
            this.album = album;
        }

        public int getReleased() {
            return released;
        }

        public void setReleased(int released) {
            this.released = released;
        }

        public List<String> getGenres() {
            return genres;
        }

        public void setGenres(List<String> genres) {
            this.genres = genres;
        }

        public String getLyrics() {
            return lyrics;
        }

        public void setLyrics(String lyrics) {
            this.lyrics = lyrics;
        }

        @Override
        public String toString() {
            String output = "Title: " + this.getTitle() + "\n";
            output += "Artist: " + this.getArtist() + "\n";
            output += "Album: " + this.getAlbum() + "\n";
            output += "Released: " + this.getReleased() + "\n";
            output += "Genres: \n";
            for (String genre : this.getGenres()) {
                output += "\t" + genre + "\n";
            }
            output += "Lyrics: \n" + this.getLyrics();


            return output;
        }
    }
}
